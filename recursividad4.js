const data=[
    {
        name:"nombre 1.1",
        children:[
            {name: "nombre 1.1.1"},
            {name:"nombre 1.1.2"}
        ]
    },
    {
        name:"nombre 1.2",
        children:[
            {name: "nombre 1.2.1"},
            {name:"nombre 1.2.2"}
        ]
    },

    {
        name:"nombre 2",
        children:[
            {name: "nombre 2.1.1"},
            {name:"nombre 2.1.2"}
        ]
    },
    {
        name:"nombre 2.2",
        children:[
            {name: "nombre 2.2.1"},
            {name:"nombre 2.2.2"}
        ]
    }
]

function print(datas){
    datas.forEach(element => {
        if(element.name){
            console.log(element.name);
            if(element.children){
                print(element.children);
            }
        }
    });
}

print(data)